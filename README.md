# Documentation #

Author: Adrian Kastrau

Computer Simulation app (written in Java language).

This app simulates process of launching computer (different types of machines e.g. desktops, notebooks and Macs)
My intention was to show some mechanisms of polymorphism and object-oriented programming in Java.


## How to compile ##

I've prepared packaged version of this app already. If you want to compile project on your own you can use:

* IntelliJ IDEA IDE to compile project or,
* Maven to compile and pack an app to jar archive.

## How to run this app ##

Just run:

     java -cp oopl-iii-project-2016-1.0-SNAPSHOT.jar pl.edu.pg.ftims.computerSimulation.App