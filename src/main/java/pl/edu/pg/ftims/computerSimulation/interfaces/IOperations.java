package pl.edu.pg.ftims.computerSimulation.interfaces;

/**
 * Some basic OS specific operations
 */
public interface IOperations {
    void createFile(String name);
    void renameFile(String name);
    void openFile(String name);
    void copyFile(String name);
    void run(String name);
    void play(String name);
    void lockMachine();
    void unlockMachine();
}
