package pl.edu.pg.ftims.computerSimulation.classes;


import pl.edu.pg.ftims.computerSimulation.exceptions.InvalidHardwareSpecificationException;
import pl.edu.pg.ftims.computerSimulation.interfaces.Launchable;

/**
 * Some basic class with a few fields to show up inheritance and polymorphism
 */
public abstract class Computer implements Launchable {
    protected String motherboardName;
    protected float cpuFreq;
    protected String cpuName;
    protected OS computerOS;
    protected int ramSize;
    protected String gpu;
    protected boolean isBootable;
    protected String machineType;
    protected boolean isValidHW;

    public Computer() {
        this.motherboardName = "Generic";
        this.cpuFreq = 1.22f;
        this.cpuName = "Generic CPU";
        this.gpu = "Integrated GPU"; //default option
        this.ramSize = 4096; //default option
        this.isBootable = false;
        this.machineType = this.getClass().getName();
        this.isValidHW = true;
    }

    public Computer(String motherboardName, float cpuFreq, String cpuName, OS computerOS) {
        if (motherboardName.trim().equals("") || cpuFreq <= 0.0f || cpuName.trim().equals(""))
            try {
                throw new InvalidHardwareSpecificationException();
            } catch (InvalidHardwareSpecificationException e) {
                e.printStackTrace();
                isValidHW = false;
            }
        this.motherboardName = motherboardName;
        this.cpuFreq = cpuFreq;
        this.cpuName = cpuName;
        this.gpu = "Integrated GPU"; //default option
        this.ramSize = 4096; //default option
        this.computerOS = computerOS;
        if (!computerOS.isInstalled()) {
           this.isBootable = false;
        } else {
            this.isBootable = true;
        }
        this.machineType = this.getClass().getName();
    }

    public OS getComputerOS() {
        return computerOS;
    }

    public void setComputerOS(OS computerOS) {
        this.computerOS = computerOS;
    }

    public String getMotherboardName() {
        return motherboardName;
    }

    public float getCpuFreq() {
        return cpuFreq;
    }

    public String getCpuName() {
        return cpuName;
    }

    public int getRamSize() {
        return ramSize;
    }

    public void setRamSize(int ramSize) throws InvalidHardwareSpecificationException {
        if (ramSize <= 0)
            throw new InvalidHardwareSpecificationException();
        this.ramSize = ramSize;
    }

    public String getGpu() {
        return gpu;
    }

    public void setGpu(String gpu) throws InvalidHardwareSpecificationException {
        if (gpu.trim().equals("")){
            throw new InvalidHardwareSpecificationException();
        }

        this.gpu = gpu;
    }

    public boolean isBootable() {
        return isBootable;
    }

    public String getMachineType() {
        return machineType;
    }

    public void launch() {
        if (isValidHW){
            System.out.println("Computer is booting up... please wait!");
            System.out.println("[Machine type: Generic computer]");
            System.out.println(this);
            boot();
        } else {
            try {
                throw new InvalidHardwareSpecificationException();
            } catch (InvalidHardwareSpecificationException e) {
                e.printStackTrace();
                return;
            }
        }

    }

    public void shutdown() throws InterruptedException {
        computerOS.shutdown();
        Thread.sleep(2500);
        System.out.println("\nComputer has been shut down!");
    }
    public abstract void boot();

    @Override
    public String toString() {
        return "Computer{" +
                "motherboardName='" + motherboardName + '\'' +
                ", cpuFreq=" + cpuFreq +
                ", cpuName='" + cpuName + '\'' +
                ", computerOS=" + computerOS +
                ", ramSize=" + ramSize +
                ", gpu='" + gpu + '\'' +
                '}';
    }
}
