package pl.edu.pg.ftims.computerSimulation.threads;

import pl.edu.pg.ftims.computerSimulation.classes.OS;

import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

public class Worker implements Runnable {
    private OS os;
    public Worker(OS os){
        this.os = os;
    }
    public void run() {
        os.setActionsPending(true);
        Iterator<String> iterator = os.getBootFiles().iterator();
        while (iterator.hasNext()){
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            iterator.next();
        }
        os.setActionsPending(false);
    }
}
