package pl.edu.pg.ftims.computerSimulation.classes;

import pl.edu.pg.ftims.computerSimulation.exceptions.InvalidHardwareSpecificationException;

public class DesktopPC extends PC {

    public DesktopPC() {
        super();
    }

    public DesktopPC(String motherboardName, float cpuFreq, String cpuName, OS computerOS) throws InvalidHardwareSpecificationException {
        super(motherboardName, cpuFreq, cpuName, computerOS);
    }
    @Override
    public void launch() {
        System.out.println("PC is booting up...");
        System.out.println("[Machine type: Desktop PC]");
        System.out.println(this);
        boot();
    }
}
