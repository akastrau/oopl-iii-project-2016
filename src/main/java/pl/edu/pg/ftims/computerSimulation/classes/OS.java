package pl.edu.pg.ftims.computerSimulation.classes;

import pl.edu.pg.ftims.computerSimulation.exceptions.FatalSystemException;
import pl.edu.pg.ftims.computerSimulation.interfaces.IOperations;
import pl.edu.pg.ftims.computerSimulation.interfaces.Launchable;

import java.util.LinkedList;
import java.util.List;

/**
 * Some generic OS - base methods
 */

public abstract class OS implements IOperations, Launchable {
    protected List<String> bootFiles;
    protected String name;
    protected boolean actionsPending;
    protected boolean installed;

    public OS() {
        bootFiles = new LinkedList<String>();
        name = "Generic";
        actionsPending = false;
        installed = false; //prevent users from using generic OS
    }

    public List<String> getBootFiles() {
        return bootFiles;
    }

    public void setBootFiles(List<String> bootFiles) {
        this.bootFiles = bootFiles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActionsPending() {
        return actionsPending;
    }

    public void setActionsPending(boolean actionsPending) {
        this.actionsPending = actionsPending;
    }

    public boolean isInstalled() {
        return installed;
    }

    public void createFile(String name) {
        System.out.println("Creating file " + name);
    }

    public void renameFile(String name) {
        System.out.println("Renaming file " + name);
    }

    public void openFile(String name) {
        System.out.println("Opening file " + name);
    }

    public void copyFile(String name)  {
        System.out.println("Copying file " + name);
    }

    public void run(String name) {
        System.out.println("Running " + name);
    }

    public void play(String name) {
        System.out.println("Playing " + name);
    }

    public void lockMachine() {System.out.println("Locking machine..."); }

    public void unlockMachine() {
        System.out.println("Unlocking machine...");
    }

    public void launch() {
        System.out.println("Starting generic OS...");
        try {
            throw new FatalSystemException();
        } catch (FatalSystemException e) {
            e.printStackTrace();
        }
    }

    public void shutdown() throws InterruptedException { System.out.println("Shutting down generic OS"); }

    public abstract void startSystem();

    @Override
    public String toString() {
        return "OS{" +
                "name='" + name + '\'' +
                '}';
    }
}
