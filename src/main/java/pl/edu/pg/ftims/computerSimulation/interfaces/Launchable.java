package pl.edu.pg.ftims.computerSimulation.interfaces;

/**
 * Interface for launching apps/os/machines etc.
 */
public interface Launchable {
    void launch();
    void shutdown() throws InterruptedException;
}
