package pl.edu.pg.ftims.computerSimulation.exceptions;


public class InvalidHardwareSpecificationException extends Exception {
    @Override
    public String getMessage() {
        return "Invalid hardware specification!";
    }
}
