package pl.edu.pg.ftims.computerSimulation.classes;

import pl.edu.pg.ftims.computerSimulation.exceptions.InvalidHardwareSpecificationException;

public class Notebook extends PC {
    public Notebook() {
        super();
    }

    public Notebook(String motherboardName, float cpuFreq, String cpuName, OS computerOS) throws InvalidHardwareSpecificationException {
        super(motherboardName, cpuFreq, cpuName, computerOS);
    }
    @Override
    public void launch() {
        System.out.println("PC is booting up...");
        System.out.println("[Machine type: Notebook]");
        System.out.println(this);
        boot();
    }
}
