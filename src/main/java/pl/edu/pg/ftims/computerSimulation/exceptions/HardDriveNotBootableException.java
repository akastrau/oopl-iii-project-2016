package pl.edu.pg.ftims.computerSimulation.exceptions;

public class HardDriveNotBootableException extends Exception {
    @Override
    public String getMessage() {
        return "Your HDD is not bootable. Please install your OS first!";
    }
}
