package pl.edu.pg.ftims.computerSimulation.interfaces;

import java.util.List;

public interface InstallSystem {
    boolean installOs(List<String> bootFiles, List<String> installerFiles);
    void prepareHDD() throws InterruptedException;
}
