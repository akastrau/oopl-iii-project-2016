package pl.edu.pg.ftims.computerSimulation.classes;

import pl.edu.pg.ftims.computerSimulation.exceptions.HardDriveNotBootableException;
import pl.edu.pg.ftims.computerSimulation.exceptions.InvalidHardwareSpecificationException;

public class Mac extends Computer {
    public Mac() {
        super();
    }

    public Mac(String motherboardName, float cpuFreq, String cpuName, OS computerOS) throws InvalidHardwareSpecificationException {
        super(motherboardName, cpuFreq, cpuName, computerOS);
    }

    @Override
    public void launch() {
        System.out.println("Mac is booting up...");
        System.out.println("[Machine type: Apple Mac]");
        System.out.println(this);
        boot();
    }

    @Override
    public void shutdown() throws InterruptedException {
        Thread.sleep(1500);
        System.out.println("The Mac has been shut down!");
    }

    public void boot() {
        System.out.println("[BOOT TYPE: APPLE-EFI]");
        if (this.isBootable){
            computerOS.startSystem();
        } else {
            try {
                throw new HardDriveNotBootableException();
            } catch (HardDriveNotBootableException e) {
                e.printStackTrace();
            }
        }
    }
}
