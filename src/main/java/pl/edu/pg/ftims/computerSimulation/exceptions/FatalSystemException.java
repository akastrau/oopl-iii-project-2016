package pl.edu.pg.ftims.computerSimulation.exceptions;

public class FatalSystemException extends Exception {
    @Override
    public String getMessage() {
        return "OS can't be used, sorry!";
    }
}
