package pl.edu.pg.ftims.computerSimulation;


import pl.edu.pg.ftims.computerSimulation.exceptions.InvalidHardwareSpecificationException;
import pl.edu.pg.ftims.computerSimulation.classes.*;

import java.util.*;

/**
 * Computer simulation
 * Simple Java app, which demonstrates some mechanisms of polymorphism
 * Adrian Kastrau
 *
 */
public class App {
    public static void main(String[] args) throws InvalidHardwareSpecificationException {
        System.out.println("\t\tAdrian Kastrau. OOP 3 project\n");
        System.out.println("\tPlease wait, we will be constructing classes" +
                " (that is to say, we are gonna install different systems on different machines)\n");
                List<Computer> computerList = new LinkedList<Computer>
                (Arrays.asList(
                        new DesktopPC("ASUS", 3.0f, "Intel", new WindowsOS()),
                        new Notebook("Lenovo", 2.1f, "Intel", new WindowsOS()),
                        new PC("Gigabyte", 1.8f, "AMD", new WindowsOS()),
                        new Mac("Apple", 2.2f, "Intel", new MacOS())));
        int counter = 1;
        HashMap<Integer, Computer> computerHashMap = new LinkedHashMap<Integer, Computer>();
        for (Computer computer : computerList){
            computerHashMap.put(counter, computer);
            counter++;
        }
        System.out.println("1 - DesktopPC, 2 - Notebook, 3 - PC, 4 - Mac");
        System.out.print("Select computer to launch: ");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        if (input <= 0 || input > computerList.size()){
            System.out.println("You chose the wrong number!");
            while (input <= 0 || input > computerList.size()){
                System.out.print("Select computer to launch: ");
                input = scanner.nextInt();
            }
        }
        computerHashMap.get(input).launch();

        System.out.print("\nWould you like to shutdown the computer? [y/n] ");
        String answer = scanner.next();
        if (answer.equalsIgnoreCase("y")){
            try {
                computerHashMap.get(input).shutdown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if (answer.equalsIgnoreCase("n")){
            System.out.println("Goodbye!");
        } else {
            System.out.println("Wrong input! Sorry!");
        }

    }
}
