package pl.edu.pg.ftims.computerSimulation.classes;

import pl.edu.pg.ftims.computerSimulation.interfaces.InstallSystem;
import pl.edu.pg.ftims.computerSimulation.threads.Worker;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MacOS extends OS implements InstallSystem {
    private List<String> installationFiles;

    public MacOS(){
        installationFiles = new LinkedList<String>(
                Arrays.<String>asList("AppleSound.kext", "HID.kext",
                        "keyboard.kext", "mouse.kext", "intelGraphics.kext",
                        "nvidiaCUDA.kext", "baseFiles.tgz", "utils.tgz"
                )
        );
        bootFiles = new LinkedList<String>(Arrays.asList("kernel.tgz", "kmod.mo", "klib.mo", "base.mo", "addons.mo"));
        if (installOs(bootFiles, installationFiles)){
            installed = true;
        } else {
            installed = false;
        }
    }

    public boolean installOs(List<String> bootFiles, List<String> installerFiles) {

        System.out.println("Launching Apple OS installer");
        Worker worker = new Worker(this);
        Thread workerThread = new Thread(worker);
        setActionsPending(true);
        System.out.print("Loading kernel and modules\n");
        workerThread.start();
        int counter = 0;
        while (isActionsPending()){
            if (counter == 3){
                System.out.print("\r");
                System.out.print("   ");
                System.out.print("\r");
                counter = 0;
            } else {
                System.out.print(".");
                counter++;
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println();
        System.out.println("Entering init 1... ");
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(3000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            prepareHDD();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Extracting system into /...");
        Double percentagePerFile = (1.0 / installerFiles.size()) * 100;
        Double percentage = 0.0;
        for (String fileName : installerFiles){
            System.out.printf("\t\tProgress: %.2f%%", percentage);
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            percentage += percentagePerFile;
            System.out.print("\r");
        }
        System.out.println("\t\tDone!                        \n");
        this.name = "Mac OS";
        return true;

    }

    public void prepareHDD() throws InterruptedException {
        System.out.print("Preparing devices... ");
        Thread.sleep(ThreadLocalRandom.current().nextInt(3000));
        System.out.println("ok");
        System.out.print("Creating partition and bootloader... ");
        Thread.sleep(ThreadLocalRandom.current().nextInt(4000));
        System.out.println("ok");
    }

    public void startSystem() {
        System.out.print("Loading kernel");
        for (String fileName : bootFiles){
            System.out.print(".");
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n\t\tMac OS loader (C) Apple Inc. ");
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(3000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("\nEntering init 1... ");
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(4000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("ok");
        System.out.println("Hello user!\n");
    }

    @Override
    public void shutdown() throws InterruptedException {
        System.out.print("Shutting down MacOS...");
    }
}
