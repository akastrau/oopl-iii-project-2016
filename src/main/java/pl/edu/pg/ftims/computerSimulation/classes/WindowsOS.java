package pl.edu.pg.ftims.computerSimulation.classes;


import pl.edu.pg.ftims.computerSimulation.interfaces.InstallSystem;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class WindowsOS extends OS implements InstallSystem {
    private List<String> installationFiles;
    private Random random;
    public WindowsOS(){
        this.actionsPending = true;
        installationFiles = Arrays.asList("explorer.exe", "advapi.dll",
                "kernel32.sys", "inet.dll", "shell32.dll", "notepad.exe",
                "calc.exe", "urlmon.dll", "nthooks.sys", "keyb.sys", "hid.sys");
        bootFiles = Arrays.asList("kernel32.sys", "installer.exe", "mscorefontsttf.exe",
                "devices.sys", "plugnplay.sys", "diskpart.exe", "user32.dll", "hal.sys");
        random = new Random();
        if (installOs(bootFiles, installationFiles)){
            installed = true;
        } else {
            installed = false;
        }

    }

    public boolean installOs(List<String> bootFiles, List<String> installerFiles) {
        System.out.println("Booting from USB drive...\n");
        System.out.println("Starting installer...");
        try {
            Thread.sleep(3500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("Loading files... ");
        for (String fileName : bootFiles){
            System.out.print(fileName + " ");
            try {
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
        System.out.println("Please wait...");
        try {
            Thread.sleep(random.nextInt(5000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("Setup is preparing HDD... ");
        prepareHDD();

        System.out.println("Setup is copying files: ");
        for (String fileName : installerFiles){
            copyFile(fileName + "          ");
            try {
                Thread.sleep(random.nextInt(750));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print("\r");
        }
        System.out.println("Successfully copied all files!");
        System.out.println("It's done!\n");
        this.name = "Windows OS";

        return true;
    }

    public void prepareHDD() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("done!");
        System.out.println();
    }

    @Override
    public void copyFile(String name) {
        System.out.print("\tCurrent file: " + name);
    }

    public void startSystem() {
        System.out.println("\n\tMicrosoft Windows OS, ver. 12.06 (C) Microsoft Corp.");
        System.out.println();
        System.out.print("Loading files... ");
        Iterator<String> iterator = bootFiles.iterator();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
            try {
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print("\nPlease wait... ");
        try {
            Thread.sleep(random.nextInt(2500));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("[OK]");
        System.out.println("Hello user!");

    }

    @Override
    public void shutdown() throws InterruptedException {
        System.out.print("Shutting down Windows...");
    }
}
