package pl.edu.pg.ftims.computerSimulation.classes;

import pl.edu.pg.ftims.computerSimulation.exceptions.HardDriveNotBootableException;
import pl.edu.pg.ftims.computerSimulation.exceptions.InvalidHardwareSpecificationException;


public class PC extends Computer {
    public PC() {
        super();
    }

    public PC(String motherboardName, float cpuFreq, String cpuName, OS computerOS) throws InvalidHardwareSpecificationException {
        super(motherboardName, cpuFreq, cpuName, computerOS);
    }

    @Override
    public void launch() {
        System.out.println("PC is booting up...");
        System.out.println("[Machine type: Generic PC]");
        System.out.println(this);
        boot();
    }

    public void boot() {
        System.out.println("[BOOT TYPE: BIOS-LEGACY]");
        if (this.isBootable){
            computerOS.startSystem();
        } else {
            try {
                throw new HardDriveNotBootableException();
            } catch (HardDriveNotBootableException e) {
                e.printStackTrace();
                return;
            }
        }
    }
}
